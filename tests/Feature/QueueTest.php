<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Teleforge\Queue\Queue;

class QueueTest extends TestCase
{
    /**
     * Tests if output is the same as the output example from the skills test document.
     *
     * @return void
     */

    public function testQueueOutput()
    {
        $queue = new Queue;
        $queue->append(10);
        $queue->prepend(5);
        $queue->prepend(90);
        $queue->append(63);
        $queue->prepend(0);
        $queue->pop();
        $queue->prepend(69);
        $queue->eject();
        $queue->pop();

        $items = $queue->items();

        $this->assertEquals($items, [90, 5, 10]);
    }
}
