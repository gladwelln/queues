## About Double-Ended Queue

A simple Double-Ended Queue which uses a Doubly Linked-List

## Installation
- git clone https://bitbucket.org/gladwelln/queues.git
- cp .env.example .env
- composer install
- php artisan key:generate

Open .env file and provide values to the following:

- SWAGGER_VERSION ([documentation](http://zircote.com/swagger-php/1.x/annotations.html#annotation-hierarchy))
- L5_SWAGGER_CONST_HOST (Configured webserver URL)

Execute below command to run queue test: 
- vendor/bin/phpunit

The above test compares output results for Input: "append 10, prepend 5, prepend 90, append 63, prepend 0, pop, prepend 69, eject, pop, show" against Output: 90, 5, 10

Execute the command to run a localhost server: php artisan serve

Access the app http://localhost:8000 and you should be presented with an API documentation.

You may also access the demo [here](https://queues.gladwelln.dev)
