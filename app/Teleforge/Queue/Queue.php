<?php

namespace App\Teleforge\Queue;

use App\Teleforge\Queue\Node;

class Queue
{
    /**
     * Collection of nodes
     *
     * @var Node[]
     */
    protected $nodes = [];

    /**
     * Queue constructor
     */
    public function __construct()
    {
        $this->nodes = $this->getNodes();
    }

    /**
     * Gets persisted nodes from session
     *
     * @return Node[]
     */
    private function getNodes()
    {
        $nodes = [];
        if (session()->has('nodes')) {
            $nodes = session()->get('nodes');
        }

        return $nodes;
    }

    /**
     * Persists nodes to session
     *
     * @param Node[] $nodes
     */
    private function updateNodes($nodes)
    {
        session()->put('nodes', $nodes);
    }

    /**
     * Adds an item to the beginning of the queue
     *
     * @param mixed $data
     *
     * @return void
     */
    public function prepend($data)
    {
        $new = $this->createNode($data);

        if ($first = $this->getFirstNode()) {
            $first->setPrevious($new);
            $new->setNext($first);
        }

        array_unshift($this->nodes, $new);

        $this->updateNodes($this->nodes);
    }

    /**
     * Adds an item to the end of the queue
     *
     * @param mixed $data
     */
    public function append($data)
    {
        $new = $this->createNode($data);

        if ($last = $this->getLastNode()) {
            $last->setNext($new);
            $new->setPrevious($last);
        }

        array_push($this->nodes, $new);

        $this->updateNodes($this->nodes);
    }

    /**
     * Removes the first item on the queue and returns it's data
     *
     * @return mixed
     */
    public function pop()
    {
        if (($removed = array_shift($this->nodes)) && !is_null($removed->getNext())) {
            $removed->getNext()->setPrevious(null);
        }

        $this->updateNodes($this->nodes);

        return $removed->getData();
    }

    /**
     * Removes the last item on the queue and returns it's data
     *
     * @return mixed
     */
    public function eject()
    {
        if (($removed = array_pop($this->nodes)) && $removed->getPrevious()) {
            $removed->getPrevious()->setNext(null);
        }

        $this->updateNodes($this->nodes);

        return $removed->getData();
    }

    /**
     * check if the queue is empty
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return count($this->nodes) === 0;
    }

    /**
     * Gets the first node
     *
     * @return Node | null
     */
    protected function getFirstNode()
    {
        $first = null;
        if (!$this->isEmpty()) {
            reset($this->nodes);
            $first = current($this->nodes);
        }

        return $first;
    }

    /**
     * Gets the last node
     *
     * @return Node | null
     */
    protected function getLastNode()
    {
        $last = null;
        if (!$this->isEmpty()) {
            end($this->nodes);
            $last = current($this->nodes);
        }

        return $last;
    }

    /**
     * Gets the list of node data
     *
     * @return mixed
     */
    public function items()
    {
        return array_map(function (Node $node) {
            return $node->getData();
        }, $this->nodes);
    }

    /**
     * Creates a new node
     *
     * @param mixed $data
     *
     * @return Node
     */
    protected function createNode($data)
    {
        return new Node($data);
    }
}
