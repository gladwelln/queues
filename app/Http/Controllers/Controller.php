<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     basePath="/",
 *      @SWG\Info(
 *         version="1.0.0",
 *         title="Queues API Explorer",
 *         description="Implementation of a Double-Ended Queue using a Doubly Linked-List",
 *         @SWG\Contact(
 *              email="gladwell_n@live.com"
 *         ),
 *     )
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
