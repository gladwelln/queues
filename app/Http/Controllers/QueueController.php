<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teleforge\Queue\Queue;
use App\Http\Requests\InputRequest;

class QueueController extends Controller
{
    /**
     * Queue object
     *
     * @var Queue
     */
    protected $queue;

    public function __construct(Queue $queue)
    {
        $this->queue = $queue;
    }

    /**
     *  @SWG\Get(
     *      path="/api/dequeue/show",
     *      operationId="queueList",
     *      tags={"Doubly Linked-List"},
     *      summary="List of node data",
     *      description="Gets the list of node data",
     *      consumes={"query string"},
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="order",
     *          description="Order by",
     *          required=false,
     *          type="string",
     *          in="query",
     *          default="asc"
     *      ),
     *      @SWG\Response(
     *          response="200",
     *          description="List of node data",
     *          examples={
     *              "application/json": {
     *                  "items"={90, 5, 10}
     *              }
     *          }
     *      )
     *  )
     */
    public function index(Request $request)
    {
        $order = $request->query('order') ?? 'asc';
        $items = $this->queue->items();

        if ($order == 'desc') {
            rsort($items);
        } else {
            sort($items);
        }

        return response()->json(['items' => $items]);
    }

    /**
     *  @SWG\Post(
     *      path="/api/dequeue/prepend",
     *      operationId="queuePrepend",
     *      tags={"Doubly Linked-List"},
     *      summary="Add item to the beginning of the queue",
     *      description="Adds an item to the beginning of the queue",
     *      consumes={"query string"},
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="input",
     *          description="Item to be added",
     *          required=true,
     *          type="string",
     *          in="query",
     *          default="fruits"
     *      ),
     *      @SWG\Response(
     *          response="422",
     *          description="Invalid input",
     *          examples={
     *              "application/json": {
     *                  "message"="The given data was invalid.",
     *                  "errors"={"input": "The input field is required."}
     *              }
     *          }
     *      ),
     *      @SWG\Response(
     *          response="201",
     *          description="Item successfully added",
     *          examples={
     *              "application/json": {
     *                  "message"="Item successfully prepended to queue!"
     *              }
     *          }
     *      )
     *  )
     */
    public function prepend(InputRequest $request)
    {
        $this->queue->prepend($request->input);

        return response()->json(['message' => 'Item successfully prepended to queue!'], 201);
    }

    /**
     *  @SWG\Post(
     *      path="/api/dequeue/append",
     *      operationId="queueAppend",
     *      tags={"Doubly Linked-List"},
     *      summary="Add item to the end of the queue",
     *      description="Adds an item to the end of the queue",
     *      consumes={"query string"},
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="input",
     *          description="Item to be added",
     *          required=true,
     *          type="string",
     *          in="query",
     *          default="fruits"
     *      ),
     *      @SWG\Response(
     *          response="422",
     *          description="Invalid input",
     *          examples={
     *              "application/json": {
     *                  "message"="The given data was invalid.",
     *                  "errors"={"input": "The input field is required."}
     *              }
     *          }
     *      ),
     *      @SWG\Response(
     *          response="201",
     *          description="Item successfully added",
     *          examples={
     *              "application/json": {
     *                  "message"="Item successfully appended to queue!"
     *              }
     *          }
     *      )
     *  )
     */
    public function append(InputRequest $request)
    {
        $this->queue->append($request->input);

        return response()->json(['message' => 'Item successfully appended to queue!'], 201);
    }

    /**
     *  @SWG\Delete(
     *      path="/api/dequeue/pop",
     *      operationId="queuePop",
     *      tags={"Doubly Linked-List"},
     *      summary="Remove the first item from the queue",
     *      description="Removes the first item on the queue and returns it's data",
     *      consumes={"query string"},
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response="404",
     *          description="No items on queue",
     *          examples={
     *              "application/json": {
     *                  "message"="No items found!"
     *              }
     *          }
     *      ),
     *      @SWG\Response(
     *          response="200",
     *          description="Item successfully removed",
     *          examples={
     *              "application/json": {
     *                  "message"="First item successfully removed!"
     *              }
     *          }
     *      )
     *  )
     */
    public function pop()
    {
        if ($this->queue->isEmpty()) {
            return response()->json(['message' => 'No items found!'], 404);
        }

        $item = $this->queue->pop();

        return response()->json(['message' => 'First item successfully removed!', 'item' => $item]);
    }

    /**
     *  @SWG\Delete(
     *      path="/api/dequeue/eject",
     *      operationId="queueEject",
     *      tags={"Doubly Linked-List"},
     *      summary="Remove the last item from the queue",
     *      description="Removes the last item on the queue and returns it's data",
     *      consumes={"query string"},
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response="404",
     *          description="No items on queue",
     *          examples={
     *              "application/json": {
     *                  "message"="No items found!"
     *              }
     *          }
     *      ),
     *      @SWG\Response(
     *          response="200",
     *          description="Item successfully removed",
     *          examples={
     *              "application/json": {
     *                  "message"="Last item successfully removed!"
     *              }
     *          }
     *      )
     *  )
     */
    public function eject()
    {
        if ($this->queue->isEmpty()) {
            return response()->json(['message' => 'No items found!'], 404);
        }

        $item = $this->queue->eject();

        return response()->json(['message' => 'Last item successfully removed!', 'item' => $item]);
    }
}
